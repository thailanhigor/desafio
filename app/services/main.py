import json
from pprint import pprint
from datetime import datetime
from dateutil import relativedelta


# def loadJson(fileName):
#     with open(f'./desafio/examples/{fileName}.json') as json_file:
#         data = json.load(json_file)
#         return data

def formatDate(date):
    return datetime.strptime(date, '%Y-%m-%d')

def calculateDiffMonths(firstDate, secondDate):
    diffMonths = relativedelta.relativedelta(firstDate,secondDate)
    return abs((diffMonths.years * 12) + diffMonths.months)

def processExperiences(json):
    computedSkills = [] 
    for experience in json:
        startDate = formatDate(experience["startDate"].split("T")[0])
        endDate =  formatDate(experience["endDate"].split("T")[0])
        diffMonths = calculateDiffMonths(endDate, startDate)
        for skill in experience["skills"]:
            flagIsNew = True
            for s in computedSkills:  
                if(skill["id"] == s.id):
                    flagIsNew = False
                    if(endDate > s.previousStartDate):
                        diffMonthsSkillExists = calculateDiffMonths(endDate, s.previousStartDate)
                        s.durationInMonths += diffMonths - diffMonthsSkillExists
                    else:
                        s.durationInMonths += diffMonths
                        
                    #update the dates of skill when  exists in list
                    s.previousStartDate = startDate
                    s.previousEndDate = endDate
    
            if(flagIsNew):
                computedSkills.append(Skill(skill["id"], skill["name"], diffMonths, startDate, endDate))

    return computedSkills


def main():
    # freelancer = Freelancerchema()
    # result = freelancer.dump(freelancers_json["freelance"])
    #obj = Freelancer(**freelancers["freelance"])
    data = loadJson("freelancer");
    freelancerExperience =  processExperiences(data["freelance"]["professionalExperiences"])
    freelancer = FreelancerSchema(id=data["freelance"]["id"], computedSkills = freelancerExperience)
    pprint(freelancer)

class FreelancerSchema():
    def __init__(self, id, computedSkills):
        self.id = id
        self.computedSkills = computedSkills
    
    def __repr__(self):
        return f"{self.id} | {self.computedSkills}"

class Skill():
    def __init__(self, id, name, durationInMonths, previousStartDate, previousEndDate):
        self.id = id
        self.name = name
        self.durationInMonths = durationInMonths
        self.previousStartDate = previousStartDate
        self.previousEndDate = previousEndDate
    
    def __repr__(self):
        return f"{self.id} | {self.name} | {self.durationInMonths}"

if(__name__ == "__main__"):
    main()