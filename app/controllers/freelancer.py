from flask_restplus import Resource, Namespace, marshal
from app.models.Freelancer import Freelancer
from flask import request
from app.serializers.freelancerSchema import freelancer_output


api = Namespace('Freelancer',description='Calculate experience in months of freelancer')

#parser documentation
parser = api.parser()
parser.add_argument('payload', location='json', help="input the correct payload format")

@api.route('')
class FreelancerEndpoint(Resource):   
    @api.response(200, "Sucess")    
    @api.response(422, "Incorrect payload format") 
    @api.response(400, "Exception Error")       
    @api.doc(description='For calculate duration in months of freelancer, you need to put freelancer json format with your skills and experience')     
    @api.expect(parser)       
    def post(self):

        #check format of json request
        try:
            json_data = request.get_json()
            data = json_data["freelance"]
        except:
            return "Incorrect payload format", 422

        try:
            freelancer = Freelancer()
            freelancer.processExperiences(data)
            return marshal(freelancer, freelancer_output, 'freelance'), 200   
        except Exception as err:
            return {"message": err.args[0]}, 400;
   
    
    
