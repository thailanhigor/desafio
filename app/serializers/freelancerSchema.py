from flask_restplus import fields

freelancer_output = {
    "id": fields.Integer,
    "computedSkills": fields.Nested({
        "id": fields.Integer,
        "name": fields.String,
        "durationInMonths": fields.Integer
    })
}
