from datetime import datetime
from dateutil import relativedelta

class Freelancer(object):
    id = 0
    computedSkills = []
    def __repr__(self):
        return f"{self.id} | {self.computedSkills}"
        
    
    def sortDescByStartDate(self, itens):
        try:
            return sorted(itens, key=lambda item: item["startDate"], reverse=True)
        except:
            raise Exception("Error when sorting the list")
   

    def formatDate(self, date):
        return datetime.strptime(date, '%Y-%m-%d')

    def calculateDiffMonths(self, firstDate, secondDate):
        diffMonths = relativedelta.relativedelta(firstDate,secondDate)
        return abs((diffMonths.years * 12) + diffMonths.months)
    
    def processExperiences(self, jsonData):
        computedSkills = [] 

        #sort the experiences to ensure the business rule works fine
        sortExperiences = self.sortDescByStartDate(jsonData["professionalExperiences"])

        try:
            for experience in sortExperiences:
                startDate = self.formatDate(experience["startDate"].split("T")[0])
                endDate =  self.formatDate(experience["endDate"].split("T")[0])
                diffMonths = self.calculateDiffMonths(endDate, startDate)
                for skill in experience["skills"]:
                    flagIsNew = True
                    for s in computedSkills:  
                        if(skill["id"] == s.id):
                            flagIsNew = False
                            if(endDate > s.previousStartDate):
                                diffMonthsSkillExists = self.calculateDiffMonths(endDate, s.previousStartDate)
                                s.durationInMonths += int(diffMonths - diffMonthsSkillExists)
                            else:
                                s.durationInMonths += int(diffMonths)
                                
                            #update the dates of skill when  exists in list
                            s.previousStartDate = startDate
                            s.previousEndDate = endDate
            
                    if(flagIsNew):
                        computedSkills.append(Skill(skill["id"], skill["name"], diffMonths, startDate, endDate))

            self.computedSkills = computedSkills
            self.id = jsonData["id"]
            
        except:
            raise Exception("Error in diff months calculation")


class Skill(object):
    def __init__(self, id, name, durationInMonths, previousStartDate, previousEndDate):
        self.id = id
        self.name = name
        self.durationInMonths = durationInMonths
        self.previousStartDate = previousStartDate
        self.previousEndDate = previousEndDate
    
    def __repr__(self):
        return f"{self.id} | {self.name} | {self.durationInMonths}"