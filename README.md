# Órama Challenge

### Description
This project is a API REST and have a only endpoint to calculate the experience difference in months of a freelancer at each company and tech skills.

## Tech
Developed in Python 3.x using Flask Restful to serve API.

## Live Demo and API Documentation
[Preview](https://oramaflask.k2lbje38logam.us-east-2.cs.amazonlightsail.com)  
For test, you can use the payload json below:
```sh
{
  "freelance": {
    "id": 42,
    "user": {
      "firstName": "Hunter",
      "lastName": "Moore",
      "jobTitle": "Fullstack JS Developer"
    },
    "status": "new",
    "retribution": 650,
    "availabilityDate": "2018-06-13T00:00:00+01:00",
    "professionalExperiences": [
			{
        "id": 80,
        "companyName": "Harber, Kirlin and Thompson",
        "startDate": "2013-05-01T00:00:00+01:00",
        "endDate": "2014-07-01T00:00:00+01:00",
        "skills": [
          {
            "id": 370,
            "name": "Javascript"
          },
          {
            "id": 400,
            "name": "Java"
          }
        ]
      },
      {
        "id": 4,
        "companyName": "Okuneva, Kerluke and Strosin",
        "startDate": "2016-01-01T00:00:00+01:00",
        "endDate": "2018-05-01T00:00:00+01:00",
        "skills": [
          {
            "id": 241,
            "name": "React"
          },
          {
            "id": 270,
            "name": "Node.js"
          },
          {
            "id": 370,
            "name": "Javascript"
          }
        ]
      },
      {
        "id": 54,
        "companyName": "Hayes - Veum",
        "startDate": "2014-01-01T00:00:00+01:00",
        "endDate": "2016-09-01T00:00:00+01:00",
        "skills": [
          {
            "id": 470,
            "name": "MySQL"
          },
          {
            "id": 400,
            "name": "Java"
          },
          {
            "id": 370,
            "name": "Javascript"
          }
        ]
      }
      
    ]
  }
}
```

## Installation
You can install using terminal and pip packages or Docker
### Using Terminal
I suppose that you have a Python 3.x installed to execute commands below.  
Clone or download this repository and execute in a terminal:
```sh
pip install -r requirements.txt
python app.py
```

Finally access your browser: http://localhost:80

### Using Docker
The image is available in: https://hub.docker.com/r/thailanhigor/oramachallenge

Check if your docker is running and execute commands:
```sh
docker pull thailanhigor/oramachallenge:v3 
docker run --name oramaContainer -p 80:80 thailanhigor/oramachallenge:v3
```
Finally access your browser: http://localhost:80


## Documentation
You can view the documentation and all endpoins available in root url: 
```sh
http://localhost:80
```
To use any endpoint, you need to put prefix: /api.
Example:
```sh
http://localhost/api/freelancer
```

## Unit Tests
The project has unit tests to ensure the correct use of API and new features.  
The tests is available in Tests folder.  
In root project directory, execute:
```sh
cd tests
python freelancerTests.py -v
```
You going to see the OK return message for all tests  


Developed by: Thailan Higor
